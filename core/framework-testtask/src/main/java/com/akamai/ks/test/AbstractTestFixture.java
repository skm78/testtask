package com.akamai.ks.test;

import org.openqa.selenium.WebDriver;

import com.akamai.ks.config.EnvironmentConfiguration;

public abstract class AbstractTestFixture {

	protected WebDriver webdriver = null;

	protected EnvironmentConfiguration environmentconfiguration = null;
	
	protected EnvironmentConfiguration getEnvironmentInfo() {
		return environmentconfiguration;
	}
	
	public void setWebDriver(WebDriver wd){
		this.webdriver = wd;
	}
	
}
