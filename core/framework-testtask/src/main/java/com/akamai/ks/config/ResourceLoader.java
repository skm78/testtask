package com.akamai.ks.config;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class ResourceLoader {

	private static final String FILE_NOT_FOUND = "File not found: \'%\'!";

	public InputStream getResource(String fileName) throws FileNotFoundException {

		InputStream in = this.getClass().getResourceAsStream("/" + fileName);

		try {
			in.available();
		} catch (Exception ex) {
			throw new FileNotFoundException(FILE_NOT_FOUND.replace("%",fileName));
		}

		return in;

	}

}
