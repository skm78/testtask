package com.akamai.ks.drivers;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.concurrent.TimeUnit;

import com.akamai.ks.config.EnvironmentConfiguration;

public class Driver {
	
	private static WebDriver driver;
	
    private Driver(){
    	
    }
	
	public static final String FIREFOX = "firefox";
	public static final String CHROME = "chrome";
	public static final String INTERNETEXPLORER = "internetexplorer";

    public static WebDriver getDriver(String driverType, EnvironmentConfiguration envconf) throws IOException{
    	
    	envconf.configureBrowsers();
    	
        	if (null==driverType || driverType.equalsIgnoreCase(FIREFOX)){
        		driver = new FirefoxDriver();
        	}else if(driverType.equalsIgnoreCase(CHROME)){
        		driver = new ChromeDriver();
        	}else if(driverType.equals(INTERNETEXPLORER)){
    			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
    			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
    			driver = new InternetExplorerDriver(ieCapabilities);        		
        	}
        	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return driver;
    }
    
}
