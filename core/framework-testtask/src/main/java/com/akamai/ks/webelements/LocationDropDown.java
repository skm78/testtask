package com.akamai.ks.webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LocationDropDown {

	private WebElement selloc;
	
    public LocationDropDown(WebElement we){
        this.selloc = we;
    }
    
    public void selectLocation(String locname){
        selloc.findElement(By.xpath("//div[@id='loc_placeholder']")).click();
        selloc.findElement(By.xpath("//li[@class='search-field']/input")).sendKeys();
        selloc.findElement(By.xpath("//div/ul/li[text()='" + locname + "']")).click();
    }

}
