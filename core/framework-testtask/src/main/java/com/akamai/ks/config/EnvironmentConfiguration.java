package com.akamai.ks.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvironmentConfiguration {

	private Logger log = LoggerFactory.getLogger(EnvironmentConfiguration.class);
	
	private String webBrowser;
	private String appUrl;
	private String geckoDriver;
	private String chromeDriver;
	private String ieDriver;
	
	private static final String WEBBROWSER_PROPERTY ="web.browser";
	private static final String APPURL_PROPERTY ="app.url";
	private static final String GECKODRIVER_PROPERTY ="webdriver.gecko.driver";
	private static final String CHROMEDRIVER_PROPERTY ="webdriver.chrome.driver";
	private static final String IEDRIVER_PROPERTY ="webdriver.ie.driver";
	
	Properties envProperties = new Properties();
	public static final String TESTTASK_PROPERTIES = "env.properties";
	
	private String returnValue(String value, String key){
	      log.debug("Configuration value for " + key + " is " + value);
	      
	      if (value == null)
	      {
	         value = envProperties.getProperty(key);
	      }
	      return value;
	   }
	
	public EnvironmentConfiguration() throws FileNotFoundException, IOException{
		
		PropertyConfigurator.configure("log4j.properties");
		envProperties.load(new ResourceLoader().getResource(TESTTASK_PROPERTIES));
		
		this.withWebBrowser(envProperties.getProperty(WEBBROWSER_PROPERTY));
		this.withAppUrl(envProperties.getProperty(APPURL_PROPERTY));
		this.withGeckoDriver(envProperties.getProperty(GECKODRIVER_PROPERTY));
		this.withChromeDriver(envProperties.getProperty(CHROMEDRIVER_PROPERTY));
		this.withIEDriver(envProperties.getProperty(IEDRIVER_PROPERTY));
		
	}
	
	
	public void configureBrowsers() throws IOException{
		String workingDirectory = System.getProperty("user.dir");
		File gecofile = new File(workingDirectory, this.getGeckoDriver());
		System.setProperty("webdriver.gecko.driver",gecofile.getCanonicalPath());
		File chromefile = new File(workingDirectory, this.getChromeDriver());
		System.setProperty("webdriver.chrome.driver",chromefile.getCanonicalPath());
		File iefile = new File(workingDirectory, this.getIEDriver());
		System.setProperty("webdriver.ie.driver",iefile.getCanonicalPath());
	}
	
	
	public EnvironmentConfiguration withWebBrowser(String value) {
		if (value != null){
	         this.webBrowser = value;    
		}
	      return this;
	}
	
	public EnvironmentConfiguration withAppUrl(String value) {
		if (value != null){
	         this.appUrl = value;    
		}
	      return this;
	}
	
	public EnvironmentConfiguration withGeckoDriver(String value) {
		if (value != null){
	         this.geckoDriver = value;    
		}
	      return this;
	}	
	
	public EnvironmentConfiguration withChromeDriver(String value) {
		if (value != null){
	         this.chromeDriver = value;    
		}
	      return this;
	}	
	
	public EnvironmentConfiguration withIEDriver(String value) {
		if (value != null){
	         this.ieDriver = value;    
		}
	      return this;
	}	
	
	public String getAppUrl(){
	      return returnValue(appUrl, APPURL_PROPERTY);
	}
	
	public String getWebBrowser(){
	      return returnValue(webBrowser, WEBBROWSER_PROPERTY);
	}		
	
	public String getGeckoDriver(){
	      return returnValue(geckoDriver, GECKODRIVER_PROPERTY);
	}
	
	public String getChromeDriver(){
	      return returnValue(chromeDriver, CHROMEDRIVER_PROPERTY);
	}
	
	public String getIEDriver(){
	      return returnValue(ieDriver, IEDRIVER_PROPERTY);
	}		
	
}
