Suggested Minimal Requirements:
-JDK 1.80_92
-Maven 3.3.1
-Web browsers: Google Chrome, FireFox, MS IE (Tested only on Chrome)
-Internet access
-Eclipse with TestNG plugin (if IDE is going to be used to run the tests)



Some Info:
- Everything  (tied via maven) is divided into:
	- framework (theoretically project independent part)
	- mode (project-specific business logic and GUI)
	- test (tests themselves)
- browsers specific exe drivers are included.
- Config is read from env.properties unless overridden by chrome_suite.xml
- In order to change config e.g: used browser, drivers' location, main url etc. - use env.properties and/or chrome_suite.xml



The tests can be run in a multiple different ways:
- Right-click on chrome_suite.xml in Eclipse and select "Run/Debug as TestNG suite."
- Execute build/run.bat or runtests\testtask\runChromeSuite.bat or simply run "mvn -U clean install" using the main pom.xml

