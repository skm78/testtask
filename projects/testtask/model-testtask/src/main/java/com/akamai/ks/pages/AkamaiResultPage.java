package com.akamai.ks.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AkamaiResultPage {
	
	final WebDriver driver;
	
	private static final String SUGGESTIONS = "Suggestions:Make sure all words are spelled correctly.Try different or more general keywords.Try a different Location or Category.";
	
	public AkamaiResultPage(WebDriver we){
		this.driver = we;
	}
	
	@FindBy(xpath = "//div[@id='jobs_filters_area']/div[@id='job_results_list_hldr'][not(@class='portal_listings')]/div[contains(@id, 'job_list_')]")
	private List<WebElement> jobOffer;
	
	@FindBy(xpath = "//div[@class='no_results_text']/p")
	private WebElement noSearchResults;
	
	
	@FindBy(xpath = "//div[@class='no_results_text']/p[text()='Suggestions:']")
	private WebElement suggestionsLabel;
	
	@FindBy(xpath = "//div[@class='no_results_text']/ul/li")
	private List<WebElement> suggestionsText;	
	
	
	public int getJobsCount(){
		return jobOffer.size();
	}
	
	private String getNoSearchResultsText(){
		return noSearchResults.getText();
	}
	
	
	private String getSuggestionsText(){
		String result = suggestionsLabel.getText();
		for (WebElement w : suggestionsText){
			result += w.getText();
		}
		return result;
	}
	
	
	public void verifySearchReturnsSomeResults(String keyword, String location){
		Assert.assertTrue(getJobsCount() > 0, "Search did not return any results.");
	}
	
	public void verifyNoSearchResultsText(String searchedphrase){
		
		Assert.assertTrue(getJobsCount() == 0, "Search did not return any results.");
		
		String title_txt = "Your search matching keyword(s) " + searchedphrase + " did not return any job results.";
		Assert.assertTrue(getNoSearchResultsText().equalsIgnoreCase(title_txt), title_txt + " did not appear on the page.");
		
		Assert.assertTrue(getSuggestionsText().equalsIgnoreCase(SUGGESTIONS), " Correct text about lack of search results did not appear.");
	}

}
