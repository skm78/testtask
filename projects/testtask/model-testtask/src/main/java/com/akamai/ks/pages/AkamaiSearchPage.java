package com.akamai.ks.pages;

import java.util.Optional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.akamai.ks.webelements.LocationDropDown;

public class AkamaiSearchPage{
	
	final WebDriver driver;

	@FindBy(xpath = "//input[@id='keyword']")
	private WebElement jobField;
	
	@FindBy(xpath = "//div[@id='jLocInputHldr']")
	private WebElement locationField;
	
	@FindBy(xpath = "//div[@id='jSearchSubmit']")
	private WebElement searchButton;
	
	public AkamaiSearchPage(WebDriver wd){
		this.driver = wd;
	}
	
	public void enterJobName(String jobname){
		jobField.sendKeys(jobname);
	}
	
	public void enterLocation(String location){
		LocationDropDown loc = new LocationDropDown(locationField);
		loc.selectLocation(location);
	}
	
	public void search(){
		searchButton.click();
	}
	
	public AkamaiResultPage searchForJob(String jobName, String location){
		enterJobName(jobName);
		if (!location.equals("")){
			enterLocation(location);	
		}
		search();	
		return PageFactory.initElements(this.driver, AkamaiResultPage.class);
	}
	
}
