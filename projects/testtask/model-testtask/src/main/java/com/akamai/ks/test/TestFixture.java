package com.akamai.ks.test;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.apache.log4j.PropertyConfigurator;

import com.akamai.ks.config.EnvironmentConfiguration;
import com.akamai.ks.drivers.Driver;
import com.akamai.ks.pages.AkamaiSearchPage;

public abstract class TestFixture extends AbstractTestFixture{
	
	protected AkamaiSearchPage searchPage;
	
	@BeforeClass
	@Parameters({"appUrl", "webBrowser", "geckoDriver", "chromeDriver", "ieDriver"})
	public void setUp(@Optional() String appUrl, @Optional() String webBrowser, @Optional() String geckoDriver, @Optional() String chromeDriver, @Optional() String ieDriver) throws FileNotFoundException, IOException{
		
	    if (null == environmentconfiguration){	
	    	environmentconfiguration = new EnvironmentConfiguration();
	    }
	    
	    PropertyConfigurator.configure("log4j.properties");

	    environmentconfiguration.withAppUrl(appUrl).withWebBrowser(webBrowser).withGeckoDriver(geckoDriver).withChromeDriver(chromeDriver).withIEDriver(ieDriver);
	    
	    setWebDriver(Driver.getDriver(environmentconfiguration.getWebBrowser(), this.environmentconfiguration));
	    this.webdriver.get(environmentconfiguration.getAppUrl());
	    
	    searchPage = searchPage();
	    
	}
	
	private AkamaiSearchPage searchPage(){
		return PageFactory.initElements(this.webdriver, AkamaiSearchPage.class);
	}
	
	
	@AfterClass
	public void Teardown(){
		Quit();
	}
	
	
	private void Quit(){
		this.webdriver.quit();
	}

}
