package com.akamai.ks;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.akamai.ks.test.TestFixture;

public class AkamaiPositiveTest extends TestFixture{
	
	@Test(dataProvider="search")
	public void positiveSearch(String keyword, String location){
		searchPage.searchForJob(keyword, location).verifySearchReturnsSomeResults(keyword,location);
	}
	
	@DataProvider(name = "search")
	public Object[][] createData() {
	 return new Object[][] {
	   { "Test", "Krakow, Poland" },
	 };
	}

}
