package com.akamai.ks;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.akamai.ks.test.TestFixture;

public class AkamaiNegativeTest extends TestFixture{
	
	@Test(dataProvider="search")
	public void negativeSearch(String keyword, String location){
		searchPage.searchForJob(keyword, location).verifyNoSearchResultsText(keyword);
	}
	
	
	@DataProvider(name = "search")
	public Object[][] createData() {
	 return new Object[][] {
	   { "XXX", "" },
	 };
	}
	
}
